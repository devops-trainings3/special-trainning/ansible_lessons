#!/bin/bash 

exit 
sudo apt update
sudo apt install software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install ansible
ansible --version


#sudo nala install ansible
ssh root@143.198.195.226 # slave02 
ssh root@152.42.169.110 # slave01



# Using adhoc command 
ansible slave01 -i inventory.ini -m ping 
ansible slave02 -i inventory.ini -m ping 
ansible workers -i inventory.ini  -m ping 
ansible localhost -i inventory.ini  -m ping 



ansible localhost -i inventory.ini -m command -a "uptime"
ansible slave02 -i inventory.ini -m command -a "uptime"


# remove this ip from known host 
ssh-keygen -R  128.199.105.85
ssh james@128.199.105.85


# verbose mode 
# adhoc command to copy file
ansible workers -i inventory.ini -m copy -a "src=/home/james/ansible_lessons/project_01/resoururces.txt dest=/var" #-vvv

## running ansible playbook 
ansible-playbook -i inventory.ini playbooks/firstplaybook.yaml

ansible-playbook -i inventory.ini playbooks/secondplaybook.yaml

ansible-playbook -i inventory.ini playbooks/dockerplaybook.yaml